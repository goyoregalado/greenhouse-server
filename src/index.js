const Gpio = require('onoff').Gpio;
const SerialPort = require('serialport');
const port = new SerialPort('/dev/ttyACM0', { baudRate: 9600 });
const LEDPin = new Gpio(4, 'out');
const fs = require('fs');
const http = require('http').createServer(function handler(req, res) {
	fs.readFile(__dirname + '/index.html', function (err, data) {
		if (err) {
			res.writeHead(500);
			return res.end('Error loading socket.io.html');
		}
		res.writeHead(200);
		res.end(data);
	});
});

const io = require('socket.io')(http)

http.listen(8080);

io.sockets.on('connection', function (socket) {
	let buttonState = 0;

	socket.on('state', function (data) {

		console.log('Niño, esto pirula', data);
		buttonState = data;
		if (buttonState != LEDPin.readSync()) {
			LEDPin.writeSync(buttonState);
		}
	});
});


port.open((err) => {
	if(err) {
		return console.log('Se ha producido un error');
	}
	port.write('Running');
});


port.on('data', (data) => {
	console.log('Data: ', data.toString());
});
